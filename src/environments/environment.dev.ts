// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `index.ts`, but if you do
// `ng build --env=prod` then `index.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const AppConfig = {
//   production: false,
//   environment: 'DEV'
// };

export const environment = {
  production: false,
  environment: 'DEV',
  baseHref: '/',
  wsBasePath: './assets/api',
  // base_url_api: 'http://127.0.0.1:8000/api'
  // base_url_api: 'https://www.techzara.org/sekoliko-api/api'
  base_url_api: 'https://www.sekoliko.org/api-teste/api'
};
