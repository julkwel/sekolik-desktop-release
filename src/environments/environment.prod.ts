// export const AppConfig = {
//   production: true,
//   environment: 'PROD'
// };
export const environment = {
  production: true,
  environment: 'PROD',
  baseHref: '/',
  wsBasePath: './assets/api',
  // base_url_api: 'http://127.0.0.1:8000/api'
  // base_url_api: 'https://www.techzara.org/sekoliko-api/api'
  base_url_api: 'https://www.sekoliko.org/api-teste/api'
};
