// export const AppConfig = {
//   production: false,
//   environment: 'LOCAL'
// };
export const environment = {
  production: false,
  baseHref: '/',
  wsBasePath: './assets/api',
  // base_url_api: 'https://www.sekoliko.org/sekoliko-api/api'
  // base_url_api: 'http://127.0.0.1:8000/api'
  base_url_api: 'https://www.sekoliko.org/api-teste/api'
};
