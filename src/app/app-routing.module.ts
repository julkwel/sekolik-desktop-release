import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {path: 'login', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', loadChildren: 'app/components/login/login.module#LoginModule'},
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'menu', loadChildren: 'app/components/Sekoliko/sekoliko.module#SekolikoModule'},
  {path: '**', redirectTo: 'login'}
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
}
