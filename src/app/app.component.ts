import {Component, OnInit} from '@angular/core';
import { ElectronService } from './providers/electron.service';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';
import {Constants} from './Utils/Constants';
import {Router} from '@angular/router';
import {AuthentificationService} from './shared/service/authentification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit {
  title = Constants.TITLE;
  public location = '' ;
  constructor(public electronService: ElectronService,
              private  _router: Router, private auth: AuthentificationService,
    private translate: TranslateService) {
    translate.setDefaultLang('en');
    console.log('AppConfig', environment);
    if (electronService.isElectron()) {
      console.log('Mode electron');
      console.log('Electron ipcRenderer', electronService.ipcRenderer);
      console.log('NodeJS childProcess', electronService.childProcess);
    } else {
      console.log('Mode web');
    }
  }
  ngOnInit() {
    this.location =  localStorage.getItem('token');
    console.log(this.location);
  }
}
