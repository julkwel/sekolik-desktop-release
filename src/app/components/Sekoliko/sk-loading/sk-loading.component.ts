import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sk-loading',
  templateUrl: './sk-loading.component.html',
  styleUrls: ['./sk-loading.component.scss']
})
export class SkLoadingComponent implements OnInit {

  loading:boolean;
  constructor() { }

  ngOnInit() {
  }

}
