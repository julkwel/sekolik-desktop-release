import {Component, OnInit, Inject} from '@angular/core';
import {Etablissement} from '../../../../shared/model/Etablissement';
import {DataService} from '../../../../shared/service/data.service';
import {ConstantHTTP} from '../../../../Utils/ConstantHTTP';
import {Router} from '@angular/router';
import {urlList} from '../../../../Utils/api/urlList';

@Component({
  selector: 'app-sk-ajout-ets',
  templateUrl: './sk-ajout-ets.component.html',
  styleUrls: ['./sk-ajout-ets.component.scss']
})
export class SkAjoutEtsComponent implements OnInit {

  sk_etablissement: Etablissement;
  loading: boolean;

  constructor(private sk_dataservice: DataService, private sk_router: Router) {
  }

  ngOnInit() {
    this.sk_etablissement = new Etablissement();
  }

  skSave(sk_etablissement: Etablissement) {

    this.loading = true;
    console.log(sk_etablissement);

    this.sk_dataservice.post(urlList.path_add_ets, sk_etablissement).subscribe(sk_response => {
      if (sk_response.code === ConstantHTTP.CODE_SUCCESS) {
        this.loading = false;
        this.sk_router.navigate(['/menu/ets']);
      }
    });

  }

}
