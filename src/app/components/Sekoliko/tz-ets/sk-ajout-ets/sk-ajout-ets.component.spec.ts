import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkAjoutEtsComponent } from './sk-ajout-ets.component';

describe('SkAjoutEtsComponent', () => {
  let component: SkAjoutEtsComponent;
  let fixture: ComponentFixture<SkAjoutEtsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkAjoutEtsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkAjoutEtsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
