export class Etablissement {
    id: number;
    nom: string;
    contact: string;
    adresse: string;
}
