import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ElectronService } from './providers/electron.service';
import { WebviewDirective } from './directives/webview.directive';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {ServiceWorkerModule} from '@angular/service-worker';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@angular/cdk/layout';
import { MatMenuModule } from '@angular/material';
import {PercentPipe, registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {AccessDeniedInterceptor} from './http-interceptors/access-denied-interceptor';
import {JwtHelperService, JwtModule} from '@auth0/angular-jwt';
import {environment} from '../environments/environment';
import {WSMain} from './shared/ws/WSMain';
import {ProjectInterceptor} from './http-interceptors/project-interceptor';
registerLocaleData(localeFr);

import {MaterialModule} from './Utils/modules/Material.module';
import {UserService} from './shared/service/user.service';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    WebviewDirective
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/ngsw-worker.js', {enabled: environment.production}),
    MDBBootstrapModule.forRoot(),
    BrowserAnimationsModule,
    LayoutModule,
    MatMenuModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    FormsModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: UserService.getAuthorizationToken,
        whitelistedDomains: ['localhost:4200', 'prfnapp01:8080'],
        blacklistedRoutes: ['localhost:4200/login', 'prfnapp01:8080/login']
      }
    }),
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    })
  ],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    ElectronService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ProjectInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AccessDeniedInterceptor,
      multi: true
    },
    WSMain,
    {provide: LOCALE_ID, useValue: 'fr-FR'},
    PercentPipe
  ],
  bootstrap: [AppComponent],
  entryComponents: []
})
export class AppModule { }
